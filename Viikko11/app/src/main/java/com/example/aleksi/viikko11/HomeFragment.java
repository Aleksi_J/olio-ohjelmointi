package com.example.aleksi.viikko11;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import static android.support.v4.content.ContextCompat.getSystemService;

public class HomeFragment extends Fragment {
    EditText et;
    TextView teksti;
    TextView disText;
    private int maxLines = 4;
    private int fontSize = 8;
    private boolean allCaps = false;
    private int typeface = Typeface.NORMAL;
    private boolean focusable = true;
    private String string;
    private String muutettu;
    private String kentta = "Lol miten tää toimii.";

    @Nullable


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        et = (EditText) view.findViewById(R.id.editText);
        teksti = (TextView) view.findViewById(R.id.textView);
        disText = (TextView) view.findViewById(R.id.textView8);
        et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        textSettings();





        return view;
    }

    private void textSettings() {
        teksti.setTextSize(fontSize);
        teksti.setMaxLines(maxLines);
        teksti.setAllCaps(allCaps);
        teksti.setTypeface(null, typeface);
        focusSettings();
        disText.setText(kentta);
    }

    private void focusSettings() {
        if (focusable == false) {
            string = muutettu;
        }
        et.setFocusable(focusable);
        if (string != null) {
            teksti.setText(string);
        }
    }

    @Override
    public void onDestroyView() {
        muutettu = et.getText().toString();
        super.onDestroyView();
    }

    public void hideKeyboard(View v) {
        InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }
    public void setFontSize(int i) {
        fontSize = i;
        //teksti.setMaxLines(i);
    }
    public void setMaxLines(int i) {
        maxLines = i;
    }
    public void capitalize(boolean isChecked){
        allCaps = isChecked;
    }
    public void setEditable(boolean isChecked){
        System.out.println(isChecked);
        focusable = isChecked;
    }

    public void setItalic(boolean isChecked){
        if (isChecked) {
            typeface = Typeface.ITALIC;
        }
        else {
            typeface = Typeface.NORMAL;
        }
    }
    public void setDText(String s) {
        kentta = s;
    }
}
