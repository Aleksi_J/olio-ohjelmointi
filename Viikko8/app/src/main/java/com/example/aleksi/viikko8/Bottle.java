package com.example.aleksi.viikko8;

import android.support.annotation.NonNull;

public class Bottle {

    private String nimi;
    private double size;
    private double price;
    public Bottle(String n, double s, double p) {
        nimi = n;
        size = s;
        price = p;
    }
    public String getName() { return nimi;}
    public double getPrice() { return price;}
    public double getSize() { return size;}
    @NonNull
    @Override
    public String toString() {
        return (nimi+ ", " + String.valueOf(price) + "€, " + String.valueOf(size) + "l");
    }
}
