package com.example.aleksi.viikko8;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;

@SuppressLint("SetTextI18n")
public class LimsaAutomaatti {
    private static LimsaAutomaatti la = new LimsaAutomaatti();
    private static ArrayList<Bottle> bottle_list = new ArrayList<>();
    private static ArrayList<Bottle> bottleBought = new ArrayList<>();
    private static double money;

    static {
        bottle_list.add(new Bottle("Pepsi Max", 0.5, 1.8));
        bottle_list.add(new Bottle("Pepsi Max", 1.5, 2.2));
        bottle_list.add(new Bottle("Coca-Cola Zero", 0.5, 2));
        bottle_list.add(new Bottle("Coca-Cola Zero", 1.5, 2.5));
        bottle_list.add(new Bottle("Fanta Zero", 0.5, 1.95));
        bottle_list.add(new Bottle("Fanta Zero", 0.5, 1.95));
    }


    private LimsaAutomaatti() {

        money = 0;


    }

    public static LimsaAutomaatti getInstance() {
        return la;
    }




    static void addMoney(TextView tv, double m) {
        money += m;
        tv.setText("Klink! Lisää rahaa laitteeseen!");

    }
    static double getMoney() { return money;}

    static void buyBottle(TextView tv, int choice) {


        if(bottle_list.isEmpty()) {
            tv.setText("Ei oo pulloi.");
        }else if (money < bottle_list.get(choice).getPrice()) {
            tv.setText("Syötä rahaa ensin!");
        }else {
            tv.setText("KACHUNK! " + bottle_list.get(choice).getName() +
                    " tipahti masiinasta!");
            money -= bottle_list.get(choice).getPrice();
            bottleBought.add(bottle_list.remove(choice));

        }

    }
    static void getCheck(Context context, TextView tv) {
        try {
            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput("kuitti.txt", Context.MODE_PRIVATE));
            ows.write("***KUITTI***\n");
            ows.write("Ostetut tuotteet:\n");
            Date dt = new Date();

            for (Bottle item: bottleBought) {
                ows.write(item.getName()+ " " + item.getSize() + "l " +item.getPrice() + "€\n");
            }
            ows.write(dt.toString()+ "\n");
            ows.write("Kiitos käytöstä!\n");
            ows.close();
            money = 0;
            tv.setText("Rahat palautettu ja kuitti tulostettu!");
            bottleBought.clear();
        } catch (FileNotFoundException e) {
            Log.e("FileNotFound", "Ei tiedostoa");
        } catch (IOException e) {
            Log.e("IOException", "Virhe syötteessä");
        }
    }


    static ArrayList<Bottle>  getBottles() {return bottle_list;}

}
