package com.example.aleksi.harkkat.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.aleksi.harkkat.R;
import com.example.aleksi.harkkat.reservation.Reservation;
import com.example.aleksi.harkkat.reservation.ReservationController;
import com.example.aleksi.harkkat.reservation.RoomReservationController;
import com.example.aleksi.harkkat.user.User;
import com.example.aleksi.harkkat.user.UserController;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


/* Prints reservations room by room. */
public class ReservationsDialog extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_textview, null);
        TextView txt = (TextView)view.findViewById(R.id.dialog_txt);
        TextView txtHead = (TextView)view.findViewById(R.id.textviewHead);
        txtHead.setText(R.string.drawer_menu_reservation);
        int userID = getArguments().getInt("userID");
        ReservationController rc = ReservationController.getInstance();
        txt.setText("");
        //lol
        

        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM");
        for (RoomReservationController room : rc.getReservationsByID(userID)) {
            txt.append(room.getName() + ":\n");
            for (Reservation res : room.getReservationArrayList()) {

                txt.append("- " + res.getEventName() + ", " + fmt.print(new DateTime(res.getStarTime())) + "\n");
            }
        }



        builder.setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });



        return builder.create();

    }
}
