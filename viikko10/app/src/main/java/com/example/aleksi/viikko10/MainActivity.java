package com.example.aleksi.viikko10;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.ListIterator;

public class MainActivity extends AppCompatActivity {

    WebView web;
    EditText searchLine;
    ArrayList<String> osoitteet = new ArrayList<>();
    ListIterator osoIter = osoitteet.listIterator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        web = (WebView) findViewById(R.id.webv);
        web.setWebViewClient(new WebViewClient());
        web.getSettings().setJavaScriptEnabled(true);
        web.loadUrl("file:///android_asset/index.html");
        searchLine = (EditText) findViewById(R.id.editText);
    }

    public void execSHOUTOUT(View v) {
        web.evaluateJavascript("javascript:shoutOut()", null);
    }

    public void execINIT(View v) {
        web.evaluateJavascript("javascript:initialize()", null);
    }

    public void searchFromWeb(View v) {
        String line = searchLine.getText().toString();

        if (!line.startsWith("http://")) {
            line = "http://" + line;
        }

        while (osoIter.hasNext()) {
            osoIter.next();
        }
        osoIter.add(line);
        System.out.println(osoitteet);
        web.loadUrl((String)  osoIter.previous());
    }

    public void prev(View v) {
        if (osoIter.hasPrevious()) {
            String prev = (String) osoIter.previous();
            web.loadUrl(prev);
        }

    }

    public void next(View v) {
        if (osoIter.hasNext()) {
            String next = (String) osoIter.next();
            System.out.println(osoitteet);
            System.out.println("lol"); 
            web.loadUrl(next);
        }

    }
    public void refresh(View v) {
        web.reload();
    }
}
