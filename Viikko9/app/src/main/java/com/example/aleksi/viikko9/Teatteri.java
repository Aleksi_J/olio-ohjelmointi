package com.example.aleksi.viikko9;

public class Teatteri {
    private String id;
    private String name;
    public Teatteri(String id, String name) {
        this.id = id;
        this.name = name;
    }
    public String getID() {
        return id;
    }

    public String toString() {
        return (name);
    }

}
