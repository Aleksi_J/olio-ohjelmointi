package com.example.aleksi.viikko11;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

public class SettingsFragment extends Fragment {
    private OnSettingsFragmentListener mCallback;
    EditText et;
    Spinner langSpinner;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        final Spinner fontSpinner = (Spinner)view.findViewById(R.id.spinner_font);
        setFontSpinner(fontSpinner);
        final Spinner lineSpinner = (Spinner)view.findViewById(R.id.spinner_lines);
        setLinesSpinner(lineSpinner);
        langSpinner = (Spinner)view.findViewById(R.id.spinner);

        Switch capSwitch = (Switch)view.findViewById(R.id.switch1);
        capSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCallback.capitalize(isChecked);
                System.out.println(isChecked);
            }
        });
        Switch italicSwitch = (Switch)view.findViewById(R.id.switch2);
        italicSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCallback.setItalic(isChecked);
                System.out.println(isChecked);
            }
        });
        Switch editSwitch = (Switch)view.findViewById(R.id.switch3);
        editSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCallback.setEditable(isChecked);

            }
        });
        et = (EditText) view.findViewById(R.id.editText2);
        et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
        Button btn = (Button)view.findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.changeLang(langSpinner.getSelectedItem().toString());
            }
        });
        return view;

    }
    public void hideKeyboard(View v) {
        InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSettingsFragmentListener) {
            mCallback = (OnSettingsFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnGreenFragmentListener");
        }
    }
    public interface OnSettingsFragmentListener {
        //void changeText(String s);
        void capitalize(boolean bool);
        void changeFont(int i);
        void maxLines(int i);
        void setItalic(boolean bool);
        void setEditable(boolean bool);
        void setDisplayText(String s);
        void changeLang(String s);
    }

    @Override
    public void onDestroyView() {
        mCallback.setDisplayText(et.getText().toString());
        super.onDestroyView();
    }

    private void setLinesSpinner(final Spinner sp) {
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCallback.maxLines(Integer.parseInt(sp.getSelectedItem().toString()));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setFontSpinner(final Spinner sp) {
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCallback.changeFont(Integer.parseInt(sp.getSelectedItem().toString()));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
