package com.example.aleksi.harkkat.json;

import com.example.aleksi.harkkat.reservation.Reservation;
import com.example.aleksi.harkkat.reservation.ReservationController;
import com.example.aleksi.harkkat.reservation.RoomReservationController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;


public class JSONReaderReservation {


    public static void readReservationStream(InputStream is) {
        try(Reader reader = new InputStreamReader(is, "UTF-8")) {
            Gson gson = new GsonBuilder().create();
            ReservationController rc = ReservationController.getInstance();
            rc.clearList();
            RoomReservationController[] rrcList = gson.fromJson(reader, RoomReservationController[].class);
            for (RoomReservationController rrc : rrcList) {
                rrc.checkID();
                for (Reservation reservation : rrc.getReservationArrayList()) {
                    reservation.checkID();
                }

                rc.addRoom(rrc);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}
