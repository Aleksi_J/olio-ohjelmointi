package com.example.aleksi.viikko9;

import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import static android.provider.AlarmClock.EXTRA_MESSAGE;


public class MainActivity extends AppCompatActivity {
    Spinner spinner;
    ArrayAdapter<Teatteri> adapter;
    XMLReader xmlReader = new XMLReader();
    Teatterit ttt;
    TextView txt;
    EditText edittext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_main);
        ttt = Teatterit.getInstance();
        xmlReader.getTheatres(ttt);
        initSpinner();
        adapter.notifyDataSetChanged();
        //txt = (TextView) findViewById(R.id.textView2);

        edittext = (EditText) findViewById(R.id.edittext);


        edittext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE && event == null) {
                    System.out.println(edittext.getText().toString());
                    InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;


                }
                return false;
            }
        });
    }
    public void sendMessage(View v) {
        Intent startNewActivity = new Intent(this, DisplayMessageActivity.class);
        EditText start =(EditText) findViewById(R.id.start);
        EditText end =(EditText) findViewById(R.id.end);
        EditText paiva =(EditText) findViewById(R.id.editText3);
        startNewActivity.putExtra("id", Teatterit.getID(spinner.getSelectedItemPosition()));
        startNewActivity.putExtra("leffa", edittext.getText().toString());
        startNewActivity.putExtra("end", end.getText().toString());
        startNewActivity.putExtra("start", start.getText().toString());
        startNewActivity.putExtra("date", paiva.getText().toString());
        //String
        //EditText theEditText = (EditText) findViewById(R.id.edit_message);
        //String message = theEditText.getText().toString();
        //startNewActivity.putExtra(EXTRA_MESSAGE, message);
        startActivity(startNewActivity);
    }


    private void initSpinner() {
        spinner = findViewById(R.id.spinner);
        adapter= new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, Teatterit.returnList());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        spinner.setAdapter(adapter);

    }/*
    public void movies(View v) {
        xmlReader.getMovies(txt, Teatterit.getID(spinner.getSelectedItemPosition()));
    }**/

}
