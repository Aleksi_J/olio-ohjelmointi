package com.example.aleksi.viikko8;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    Button bt, buttonBuy;
    SeekBar sb;
    LimsaAutomaatti lol;
    TextView seekBarValue;
    TextView moneyInserted;
    TextView terminal;
    ArrayAdapter<Bottle> adapter;
    Spinner spinner;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lol = LimsaAutomaatti.getInstance();
        sb = findViewById(R.id.seekBar);
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekBarValue.setText(String.valueOf(progress*0.5));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        bt = findViewById(R.id.add);
        context = MainActivity.this;
        buttonBuy = findViewById(R.id.buttonBuy);
        seekBarValue = findViewById(R.id.textView2);
        moneyInserted = findViewById(R.id.textView);
        moneyInserted.setText(String.valueOf(LimsaAutomaatti.getMoney()));
        terminal = findViewById(R.id.terminal);
        initSpinner();
    }

    public void addMoney(View v) {

        LimsaAutomaatti.addMoney(terminal,Double.parseDouble(seekBarValue.getText().toString()));
        moneyInserted.setText(String.valueOf(LimsaAutomaatti.getMoney()));
        sb.setProgress(0);

    }
    public void BuyBottle(View v) {
        LimsaAutomaatti.buyBottle(terminal, spinner.getSelectedItemPosition());
        moneyInserted.setText(String.valueOf(LimsaAutomaatti.getMoney()));
        adapter.notifyDataSetChanged();
        spinner.setSelection(0);
    }
    private void initSpinner() {
        spinner = findViewById(R.id.spinner);
        adapter= new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, LimsaAutomaatti.getBottles());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        spinner.setAdapter(adapter);

    }
    public void check( View v) {
        LimsaAutomaatti.getCheck(context, terminal);
        moneyInserted.setText(String.valueOf(LimsaAutomaatti.getMoney()));

    }



}
