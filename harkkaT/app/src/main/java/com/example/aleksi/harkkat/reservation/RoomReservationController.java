package com.example.aleksi.harkkat.reservation;

import android.util.NoSuchPropertyException;

import com.example.aleksi.harkkat.exceptions.DuplicateException;
import com.example.aleksi.harkkat.reservation.Reservation;
import com.example.aleksi.harkkat.user.User;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Date;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class RoomReservationController {
    private static ArrayList<Integer> roomIDList = new ArrayList<>();
    private String name;
    private String location;
    private boolean frozen;
    private ArrayList<Reservation> reservationArrayList = new ArrayList<>();
    private ArrayList<Reservation> tempList = new ArrayList<>();
    private int roomID;
    private int maxCapacity;




    public RoomReservationController(String name, String location, boolean frozen, int maxCapacity) {
        this.name = name;
        this.location = location;
        this.frozen = frozen;
        this.roomID = setID();
        this.maxCapacity = maxCapacity;
    }

    public RoomReservationController(String name, String location, boolean frozen, int maxCapacity, int roomID) {
        this.name = name;
        this.location = location;
        this.frozen = frozen;
        this.roomID = roomID;
        roomIDList.add(roomID);
        this.maxCapacity = maxCapacity;
    }


    private int setID() {
        int id = 0;
        while (roomIDList.contains(id)) {
            id++;
        }
        roomIDList.add(id);
        return id;
    }

    public void checkID() {
        if(!roomIDList.contains(this.roomID)) {
            roomIDList.add(this.roomID);
        }
    }

    public ArrayList<Reservation> getReservationArrayList() {
        return reservationArrayList;
    }



    public boolean addReservation (Reservation reservation) throws IndexOutOfBoundsException,
    IllegalAccessException{
        ListIterator listIterator = reservationArrayList.listIterator();

        if(reservation.getCapacity() > maxCapacity) {
            throw new IndexOutOfBoundsException("Reservation's capacity bigger than rooms capacity.");
        }
        if(isFrozen())
            throw new IllegalAccessException("Room is frozen");
        if (reservation.getStarTime().after(reservation.getEndTime()))
            return false;


        while (listIterator.hasNext()) {
            Reservation next = (Reservation)listIterator.next();
            if (reservation.getEndTime().before(next.getStarTime())) {
                try {
                    listIterator.previous();
                    Reservation prev = (Reservation) listIterator.previous();
                    if (reservation.getStarTime().after(prev.getEndTime())) {
                        listIterator.next();
                        listIterator.add(reservation);
                        return true;
                    }
                    return false;
                } catch (NoSuchElementException ex) {
                    listIterator.next();
                    listIterator.add(reservation);
                    return true;
                }

            }
        }
        try {
            Reservation prev = (Reservation) listIterator.previous();
            if (reservation.getStarTime().after(prev.getEndTime())) {
                listIterator.next();
                listIterator.add(reservation);
                return true;
            }
        } catch (NoSuchElementException ex) {
            listIterator.add(reservation);
            return true;
        }
        return false;
    }

    public boolean removeReservation(int eventID, User user) {
        for (Reservation reservation: reservationArrayList) {
            if (reservation.getEventID() == eventID) {
                if (reservation.getReserverID() == user.getUserID() || user.isSuperUser() ) {
                    reservationArrayList.remove(reservation);
                    return true;
                }
            }
        }
        return false;
    }




    public ArrayList<Reservation> getReservationsByID(int id) {
        tempList.clear();
        for (Reservation reservation: reservationArrayList) {
            if (reservation.getReserverID() == id) {
                tempList.add(reservation);
            }
        }
        return tempList;
    }


    public void removeAllByID(int id) {
        ListIterator<Reservation> iterator = reservationArrayList.listIterator();
        while (iterator.hasNext()) {
            Reservation temp = iterator.next();
            if (temp.getReserverID() == id) {
                iterator.remove();
            } else if (temp.checkParticipant(id)) {
                temp.removeParticipant(id);
            }
        }


    }





    public ArrayList<Reservation> getParticipationsByID(int id) {
        tempList.clear();
        for (Reservation reservation: reservationArrayList) {
            if (reservation.checkParticipant(id)) {
                tempList.add(reservation);
            }
        }
        return tempList;
    }

    public ArrayList<Reservation> getParticipationsByIDAndTime(int id, Date date) {
        tempList.clear();
        for (Reservation reservation: reservationArrayList) {
            if (reservation.checkParticipant(id)) {

                DateTime f = new DateTime(reservation.getStarTime());
                DateTime dateTime = new DateTime(date);

                LocalDate first = f.toLocalDate();
                LocalDate second = dateTime.toLocalDate();

                if(first.compareTo(second) == 0) {
                    tempList.add(reservation);
                }
            }
        }
        return tempList;
    }


    public ArrayList<Reservation> getReservationsByIDAndTime(int id, DateTime date) {
        tempList.clear();
        for (Reservation reservation: reservationArrayList) {
            if (reservation.getReserverID() == id) {

                DateTime f = new DateTime(reservation.getStarTime());
                DateTime dateTime = new DateTime(date);

                LocalDate first = f.toLocalDate();
                LocalDate second = dateTime.toLocalDate();

                if(first.compareTo(second) == 0) {
                    tempList.add(reservation);
                }
            }
        }
        return tempList;
    }

    public ArrayList<Reservation> getReservationsByTime(DateTime date) {
        tempList.clear();
        for (Reservation reservation: reservationArrayList) {

                DateTime f = new DateTime(reservation.getStarTime());
                DateTime dateTime = new DateTime(date);

                LocalDate first = f.toLocalDate();
                LocalDate second = dateTime.toLocalDate();

                if(first.compareTo(second) == 0) {
                    tempList.add(reservation);
                }
            }

        return tempList;
    }



    public boolean changeReserver(int eventID, int newReserver) {
        try {
            Reservation res = getResByID(eventID);
            res.setReserverID(newReserver);
            return true;
        } catch (NoSuchPropertyException e) {
            return false;
        }
    }


    public boolean changeEventName(int eventID, String newName) {
        try {
            Reservation res = getResByID(eventID);
            res.setEventName(newName);
            return true;
        } catch (NoSuchPropertyException e) {
            return false;
        }
    }




    public void participateToEvent(int eventID, int participant) throws IndexOutOfBoundsException, DuplicateException,
    NoSuchPropertyException{
        try {
            Reservation res = getResByID(eventID);
            res.addParticipant(participant);

        } catch (NoSuchPropertyException e) {
            throw new NoSuchPropertyException("");

        } catch (IndexOutOfBoundsException e) {
            throw new IndexOutOfBoundsException("There is no room in this event.");
        } catch (DuplicateException e) {
            throw new DuplicateException("User already here.");
        }
    }

    public boolean removeParticipation(int eventID, int participant) {
        try {
            Reservation res = getResByID(eventID);
            if (res.removeParticipant(participant))
                return true;

        } catch (NoSuchPropertyException e) {
            return false;
        }
        return false;
    }



    public Reservation getResByID(int id) throws NoSuchPropertyException {
        for(Reservation res:reservationArrayList) {
            if (res.getEventID() == id) {
                return res;
            }
        }
        throw new NoSuchPropertyException("There is no such reservation.");
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public boolean isFrozen() {
        return frozen;
    }

    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    public int getRoomID() {
        return roomID;
    }

    public void setReservationArrayList(ArrayList<Reservation> reservationArrayList) {
        this.reservationArrayList = reservationArrayList;
    }

    @Override
    public String toString() {
        return name;
    }
}
