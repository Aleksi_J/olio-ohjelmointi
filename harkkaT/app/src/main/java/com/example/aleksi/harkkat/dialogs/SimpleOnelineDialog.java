package com.example.aleksi.harkkat.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.aleksi.harkkat.R;

/*Dialog with one EditText that returns its string.*/
public class SimpleOnelineDialog extends DialogFragment {
    private EditText etxt;
    OneLineListener listener ;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_oneline, null);

        etxt = (EditText)view.findViewById(R.id.oneline_dialog_etxt);
        String start = getArguments().getString("etxt");
        int i = getArguments().getInt("cap");
        if(i == 1)
            etxt.setInputType(InputType.TYPE_CLASS_NUMBER);
        etxt.setText(start);

        listener = (SimpleOnelineDialog.OneLineListener)getTargetFragment();

        builder.setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!etxt.getText().toString().equals(""))
                            listener.set(etxt.getText().toString());

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SimpleOnelineDialog.this.getDialog().cancel();
            }
        });
        return builder.create();
    }




    public interface OneLineListener {
        void set(String s);

    }
}
