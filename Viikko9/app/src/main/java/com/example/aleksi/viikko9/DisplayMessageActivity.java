package com.example.aleksi.viikko9;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DisplayMessageActivity extends AppCompatActivity {
    private EditText edittext;
    Context context;
    TextView txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);
        Intent intent = getIntent();
        txt = (TextView) findViewById(R.id.textView2);
        XMLReader xmlReader = new XMLReader();

        xmlReader.getMovies(txt,
                intent.getStringExtra("id"),
                intent.getStringExtra("leffa"),
                intent.getStringExtra("date"),
                intent.getStringExtra("start"),
                intent.getStringExtra("end"));

    }





}
