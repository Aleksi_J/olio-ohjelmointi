package com.example.aleksi.harkkat.reservation;

import android.util.NoSuchPropertyException;

import java.util.ArrayList;

public class ReservationController {
    private static final ReservationController ourInstance = new ReservationController();
    private static ArrayList<RoomReservationController> roomList = new ArrayList<>();
    public static ReservationController getInstance() {
        return ourInstance;
    }

    private ReservationController() {
    }


    public void addRoom(RoomReservationController room) {
        roomList.add(room);
    }


    public void removeRoom(int roomID) {
        RoomReservationController room = this.getRoom(roomID);
        roomList.remove(room);

    }

    public ArrayList<RoomReservationController> getReservationsByID(int reserverID) {
        ArrayList<RoomReservationController> temp = new ArrayList<>();
        for (RoomReservationController room : roomList) {
            RoomReservationController tempo = new RoomReservationController(room.getName(),room.getLocation(),room.isFrozen(),room.getMaxCapacity());
            tempo.setReservationArrayList(room.getReservationsByID(reserverID));
            temp.add(tempo);
        }
        return temp;
    }

    public ArrayList<RoomReservationController> getParticipationsByID(int reserverID) {
        ArrayList<RoomReservationController> temp = new ArrayList<>();
        for (RoomReservationController room : roomList) {
            RoomReservationController tempo = new RoomReservationController(room.getName(),room.getLocation(),room.isFrozen(),room.getMaxCapacity());
            tempo.setReservationArrayList(room.getParticipationsByID(reserverID));
            temp.add(tempo);
        }
        return temp;
    }

    public RoomReservationController getRoom(int roomID) throws NoSuchPropertyException {
        for(RoomReservationController room: roomList) {
            if(room.getRoomID() == roomID) {
                return room;
            }
        }

        throw new NoSuchPropertyException("There is no room with this id: " + roomID);
    }

    public void removeAllByID(int id) {
        for (RoomReservationController rrc : roomList) {
            rrc.removeAllByID(id);
        }
    }


    public static ArrayList<RoomReservationController> getRoomList() {
        return roomList;
    }
    public static void clearList() {
        roomList.clear();
    }


}
