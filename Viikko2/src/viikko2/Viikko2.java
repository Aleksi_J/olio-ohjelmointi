/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aleksi
 */
public class Viikko2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        String nimi = "";
        System.out.print("Anna koiralle nimi: ");
        
        try  {
            nimi = br.readLine();
        } catch (IOException ex) {
            System.out.println("VIRHE!");
        }
        Dog doge1 = new Dog(nimi);

        
        System.out.println("Hei, nimeni on " + doge1.getName());
        System.out.print("Mitä koira sanoo: ");
        
        try {
            doge1.speak(br.readLine());
        } catch (IOException ex) {
            System.out.println("VIRHE!");
        }
        
        
    }
    
}
