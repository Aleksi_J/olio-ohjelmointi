package com.example.aleksi.harkkat.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.NoSuchPropertyException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aleksi.harkkat.R;
import com.example.aleksi.harkkat.dialogs.RoomAddDialog;
import com.example.aleksi.harkkat.dialogs.SimpleOnelineDialog;
import com.example.aleksi.harkkat.json.JSONWriterReservation;
import com.example.aleksi.harkkat.reservation.ReservationController;
import com.example.aleksi.harkkat.reservation.RoomReservationController;

import java.io.IOException;

public class RoomFragment extends Fragment implements SimpleOnelineDialog.OneLineListener {
    private Switch superUserSwitch;
    private Switch frozenSwitch;
    private Button locationButton;
    private Button nameButton;
    private Button addButton;
    private Button deleteButton;
    private Button capasityButton;
    private Button extraButton;
    private TextView capasityText;
    private TextView nameText;
    private TextView locationText;
    private Spinner spinner;
    private ArrayAdapter<RoomReservationController> adapter;
    private RoomReservationController room;
    private int changeSet;
    private final int NAME = 1;
    private final int LOCATION = 2;
    private final int CAPASITY = 3;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.super_user_template, container, false);

        setComponents(view);
        initUserSpinner(spinner);
        initSwitches();
        initButtons();

        return view;
    }




    private void setComponents(View view) {
        spinner = (Spinner)view.findViewById(R.id.superUser_spinner);
        nameButton = (Button)view.findViewById(R.id.superUser_name);
        nameButton.setText(R.string.change);
        locationButton = (Button)view.findViewById(R.id.superUser_sec);
        locationButton.setText(R.string.change);
        deleteButton = (Button)view.findViewById(R.id.superUser_delete);
        deleteButton.setText(R.string.delete);
        addButton = (Button)view.findViewById(R.id.superUser_add);
        addButton.setText(R.string.add);
        frozenSwitch = (Switch)view.findViewById(R.id.superUser_switch2);
        frozenSwitch.setText(R.string.frozen);
        superUserSwitch = (Switch)view.findViewById(R.id.superUser_Switch1);
        superUserSwitch.setVisibility(View.INVISIBLE);
        nameText = (TextView)view.findViewById(R.id.superUser_nameT);
        locationText = (TextView)view.findViewById(R.id.superUser_sect);
        capasityButton = (Button)view.findViewById(R.id.superUser_cap);
        capasityText = (TextView)view.findViewById(R.id.superUser_capt);
        extraButton = (Button)view.findViewById(R.id.resButton);
        extraButton.setVisibility(View.INVISIBLE);

    }

    private void initButtons() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addRoom();
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeRoom();
            }
        });

        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog(room.getLocation());
                changeSet = LOCATION;
            }
        });

        nameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog(room.getName());
                changeSet = NAME;
            }
        });
        capasityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleOnelineDialog dialog = new SimpleOnelineDialog();
                dialog.setTargetFragment(RoomFragment.this, 0);
                Bundle args = new Bundle();
                args.putString("etxt", Integer.toString(room.getMaxCapacity()));
                args.putInt("cap", 1);
                dialog.setArguments(args);
                dialog.show(getActivity().getSupportFragmentManager(),"lol");
                changeSet = CAPASITY;
            }
        });
    }

    private void initUserSpinner(final Spinner spinner) {
        ReservationController rc = ReservationController.getInstance();
        adapter = new ArrayAdapter<>(getContext().getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item, rc.getRoomList());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                room = (RoomReservationController) parent.getSelectedItem();
                nameText.setText(R.string.name);
                nameText.append(room.getName());
                locationText.setText(R.string.Location);
                locationText.append(room.getLocation());
                frozenSwitch.setChecked(room.isFrozen());
                capasityText.setText(R.string.capacity);
                capasityText.append(Integer.toString(room.getMaxCapacity()));
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void initSwitches() {
        frozenSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (room != null) {
                    room.setFrozen(isChecked);
                    writeChanges();
                }
            }
        });
    }
    private void writeChanges() {
        try {
            JSONWriterReservation.writeReservationStream(ReservationController.getRoomList(), getContext());
        } catch (IOException e) {
            Toast.makeText(getContext(), R.string.write_failed, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void addRoom() {
        RoomAddDialog dialog = new RoomAddDialog();
        dialog.show(getActivity().getSupportFragmentManager(),"lol");
        adapter.notifyDataSetChanged();
    }
    private void removeRoom() {
        ReservationController reservationController = ReservationController.getInstance();
        try {
            reservationController.removeRoom(room.getRoomID());
            adapter.notifyDataSetChanged();
            writeChanges();
        } catch (NoSuchPropertyException e) {
            Toast.makeText(getContext(), R.string.remove_failure, Toast.LENGTH_SHORT).show();
        }
    }

    public void setDialog(String s) {
        SimpleOnelineDialog dialog = new SimpleOnelineDialog();
        dialog.setTargetFragment(this, 0);
        Bundle args = new Bundle();
        args.putString("etxt", s);
        dialog.setArguments(args);
        dialog.show(getActivity().getSupportFragmentManager(),"lol");

    }


    /*Sets new values for room's variable.*/
    @Override
    public void set(String s) {
        switch (changeSet) {
            case LOCATION:
                room.setLocation(s);
                locationText.setText(R.string.Location);
                locationText.append(room.getLocation());
                break;
            case NAME:
                room.setName(s);
                nameText.setText(R.string.name);
                nameText.append(room.getName());
                break;
            case CAPASITY:
                room.setMaxCapacity(Integer.parseInt(s));
                capasityText.setText(R.string.capacity);
                capasityText.append(Integer.toString(room.getMaxCapacity()));

        }
        writeChanges();
    }
}
