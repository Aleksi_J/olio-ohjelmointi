package com.example.aleksi.harkkat.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aleksi.harkkat.R;
import com.example.aleksi.harkkat.dialogs.ReservationsDialog;
import com.example.aleksi.harkkat.dialogs.SignInDialog;
import com.example.aleksi.harkkat.dialogs.SimpleOnelineDialog;
import com.example.aleksi.harkkat.exceptions.DuplicateException;
import com.example.aleksi.harkkat.json.JSONWriterReservation;
import com.example.aleksi.harkkat.json.JSONWriterUser;
import com.example.aleksi.harkkat.reservation.ReservationController;
import com.example.aleksi.harkkat.user.User;
import com.example.aleksi.harkkat.user.UserController;

import java.io.IOException;

public class UserFragment extends Fragment implements SimpleOnelineDialog.OneLineListener {
    private Switch superUserSwitch;
    private Switch frozenSwitch;
    private Button passwdButton;
    private Button nameButton;
    private Button addButton;
    private Button deleteButton;
    private Button extraButton;
    private TextView extraTextview;
    private TextView nameText;
    private TextView passwdText;
    private Spinner spinner;
    private Button resButton;
    private ArrayAdapter<User> adapter;
    private User user;
    private int changeSet;
    private final int NAME = 1;
    private final int PASSWORD = 2;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.super_user_template, container, false);

        setComponents(view);
        initUserSpinner(spinner);
        initSwitches();
        initButtons();

        return view;
    }




    private void setComponents(View view) {
        spinner = (Spinner)view.findViewById(R.id.superUser_spinner);
        nameButton = (Button)view.findViewById(R.id.superUser_name);
        nameButton.setText(R.string.change);
        passwdButton = (Button)view.findViewById(R.id.superUser_sec);
        passwdButton.setText(R.string.change);
        deleteButton = (Button)view.findViewById(R.id.superUser_delete);
        deleteButton.setText(R.string.delete);
        addButton = (Button)view.findViewById(R.id.superUser_add);
        addButton.setText(R.string.add);
        frozenSwitch = (Switch)view.findViewById(R.id.superUser_switch2);
        frozenSwitch.setText(R.string.frozen);
        superUserSwitch = (Switch)view.findViewById(R.id.superUser_Switch1);
        superUserSwitch.setText(R.string.superuser);
        nameText = (TextView)view.findViewById(R.id.superUser_nameT);
        passwdText = (TextView)view.findViewById(R.id.superUser_sect);
        extraButton = (Button)view.findViewById(R.id.superUser_cap);
        extraButton.setVisibility(View.INVISIBLE);
        extraTextview = (TextView)view.findViewById(R.id.superUser_capt);
        extraTextview.setVisibility(View.INVISIBLE);
        resButton = (Button)view.findViewById(R.id.resButton);
    }

    private void initButtons() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addUser();
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeUser();
            }
        });

        passwdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog(user.getPasswd());
                changeSet = PASSWORD;
            }
        });

        nameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDialog(user.getName());
                changeSet = NAME;
            }
        });
        resButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seeReservations();
            }
        });
    }

    private void initUserSpinner(final Spinner spinner) {
        UserController uc = UserController.getInstance();
        adapter = new ArrayAdapter<>(getContext().getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item, uc.getUserArrayList());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                user = (User)parent.getSelectedItem();
                nameText.setText(R.string.name);
                nameText.append(user.getName());
                passwdText.setText(R.string.passwd);
                passwdText.append(user.getPasswd());
                frozenSwitch.setChecked(user.isFrozen());
                superUserSwitch.setChecked(user.isSuperUser());
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void initSwitches() {
        superUserSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(user != null) {
                    user.setSuperUser(isChecked);
                    writeChanges();
                }
            }
        });
        frozenSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (user != null) {
                    user.setFrozen(isChecked);
                    writeChanges();
                }
            }
        });
    }
    private void writeChanges() {
        try {
            JSONWriterUser.writeUsers("users.json", getContext());
        } catch (IOException e) {
            Toast.makeText(getContext(), R.string.write_failed, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void addUser() {
        SignInDialog dialog = new SignInDialog();
        dialog.show(getActivity().getSupportFragmentManager(), "tag");
    }


    /*Shows users reservations.*/
    private void seeReservations() {
        ReservationsDialog dialog = new ReservationsDialog();
        Bundle args = new Bundle();
        args.putInt("userID", user.getUserID());
        dialog.setArguments(args);
        dialog.show(getActivity().getSupportFragmentManager(), "lolz");
    }

    private void removeUser() {
        UserController userController = UserController.getInstance();
        ReservationController rc = ReservationController.getInstance();
        rc.removeAllByID(user.getUserID());
        userController.removeUserByID(user.getUserID());
        adapter.notifyDataSetChanged();
        writeChanges();
        try {
            JSONWriterReservation.writeReservationStream(rc.getRoomList(),getContext());
        } catch (IOException e) {
            Toast.makeText(getContext(),R.string.write_failed, Toast.LENGTH_SHORT).show();
        }
    }


    /*dialog for value change.*/
    public void setDialog(String s) {
        SimpleOnelineDialog dialog = new SimpleOnelineDialog();
        dialog.setTargetFragment(this, 0);
        Bundle args = new Bundle();
        args.putString("etxt", s);
        dialog.setArguments(args);
        dialog.show(getActivity().getSupportFragmentManager(),"lol");

    }


    /*Changing the value.*/
    @Override
    public void set(String s) {
        switch (changeSet) {
            case PASSWORD:
                user.setPasswd(s);
                passwdButton.setText(R.string.passwd);
                passwdText.append(user.getPasswd());
                break;
            case NAME:
                UserController controller = UserController.getInstance();
                try {
                    controller.changeName(user, s);
                    nameText.setText(R.string.name);
                    nameText.append(user.getName());
                } catch (DuplicateException e) {
                    Toast.makeText(getContext(), R.string.username_used, Toast.LENGTH_SHORT).show();
                }
        }
        writeChanges();
    }
}
