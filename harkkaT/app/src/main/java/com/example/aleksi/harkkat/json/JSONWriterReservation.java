package com.example.aleksi.harkkat.json;

import android.content.Context;

import com.example.aleksi.harkkat.reservation.RoomReservationController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class JSONWriterReservation {
    public static void writeReservationStream(ArrayList<RoomReservationController> rrc, Context con) throws IOException {
        //Writer writer = new FileWriter("output.json");
        OutputStreamWriter writer = new OutputStreamWriter(con.openFileOutput("output.json",Context.MODE_PRIVATE));

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm'Z'").create();
        gson.toJson(rrc, writer);
        writer.close();
    }
}
