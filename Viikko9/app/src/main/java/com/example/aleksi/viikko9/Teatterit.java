package com.example.aleksi.viikko9;

import java.util.ArrayList;

public class Teatterit {
    private static Teatterit ttt= new Teatterit();
    private static ArrayList<Teatteri> lista= new ArrayList<>();


    private Teatterit() {


    }

    public static Teatterit getInstance() {
        return ttt;
    }

    protected static void addTheatre(String id, String name) {
        lista.add(new Teatteri(id, name));
    }

    protected static ArrayList<Teatteri> returnList() {
        return lista;
    }
    protected static String getID(int i) {

        return lista.get(i).getID();
    }
    protected static void clear() {
        lista.clear();
    }

}
