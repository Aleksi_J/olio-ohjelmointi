package com.example.aleksi.viikko9;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class XMLReader {

    public void getTheatres(Teatterit t) {
        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String urlString = "https://www.finnkino.fi/xml/TheatreAreas";
            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();
            System.out.println(doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getDocumentElement().getElementsByTagName("TheatreArea");
            Teatterit.clear();
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    Teatterit.addTheatre(element.getElementsByTagName("ID").item(0).getTextContent(),
                            element.getElementsByTagName("Name").item(0).getTextContent());
                }
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
            System.out.println("*******DONE*******");
        }
    }


    public void getMovies(TextView txt, String id, String leffa, String date, String start, String end) {
        DocumentBuilder builder = null;
        if (date == null) {
            Date nykyinen = new Date();
            date = new SimpleDateFormat("d.M.yyyy").format(nykyinen);
        }
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String urlString = "https://www.finnkino.fi/xml/Schedule/?area="+id+"&dt=" + date;
            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getDocumentElement().getElementsByTagName("Show");
            txt.setText("");
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    //System.out.println(leffa);
                    //IlSystem.out.println(element.getElementsByTagName("Title").item(0).getTextContent());
                    if(leffa.equals("")) {
                        if(checkTime(start,end, element.getElementsByTagName("dttmShowStart").item(0).getTextContent())) {
                            txt.append(element.getElementsByTagName("Title").item(0).getTextContent());
                            txt.append("   "+ element.getElementsByTagName("dttmShowStart").item(0).getTextContent().substring(11,16) + "   ");
                            txt.append("\t" + element.getElementsByTagName("LengthInMinutes").item(0).getTextContent() + "min\n");
                        }
                    }
                    else {
                        if (leffa.toLowerCase().equals(element.getElementsByTagName("Title").item(0).getTextContent().toLowerCase())) {
                            if(checkTime(start,end, element.getElementsByTagName("dttmShowStart").item(0).getTextContent())) {
                                txt.append(element.getElementsByTagName("Title").item(0).getTextContent());
                                txt.append("   "+ element.getElementsByTagName("dttmShowStart").item(0).getTextContent().substring(11,16) + "   ");
                                txt.append("\t" + element.getElementsByTagName("LengthInMinutes").item(0).getTextContent() + "min\n");
                            }
                        }
                    }
                }
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } finally {
        }
    }

    private boolean checkTime(String start, String End, String leffaTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        try {
            System.out.println(leffaTime.substring(11, 16));
            if (start.equals("")) {
                start = "00:00";
            }
            if (End.equals("")) {
                End = "23:59";
            }
            Date startt = formatter.parse(start);
            Date endt = formatter.parse(End);
            Date leffa = formatter.parse(leffaTime.substring(11, 16));
            if(leffa.before(endt) && leffa.after(startt)) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

}
