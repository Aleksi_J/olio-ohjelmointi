package com.example.aleksi.harkkat.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.aleksi.harkkat.R;
import com.example.aleksi.harkkat.exceptions.DuplicateException;
import com.example.aleksi.harkkat.json.JSONWriterUser;
import com.example.aleksi.harkkat.user.User;
import com.example.aleksi.harkkat.user.UserController;

import java.io.IOException;

/*sign in dialog.*/
public class SignInDialog extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_sign_in, null);

        builder.setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        UserController userController = UserController.getInstance();
                        EditText txt = (EditText) view.findViewById(R.id.dialog_name);
                        EditText passwd = (EditText) view.findViewById(R.id.dialog_passwd);
                        User user = new User(txt.getText().toString(),false, false,passwd.getText().toString());
                        try {
                            if(userController.addUser(user)) {
                                Toast.makeText(getContext(), R.string.dialog_user_added, Toast.LENGTH_LONG).show();
                                    JSONWriterUser.writeUsers("users.json", getContext());
                            }
                            else {
                                Toast.makeText(getContext(), R.string.dialog_user_failed, Toast.LENGTH_LONG).show();
                            }
                        } catch (DuplicateException e) {
                            Toast.makeText(getContext(), R.string.dialog_user_failed, Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            Toast.makeText(getContext(), R.string.write_failed, Toast.LENGTH_LONG).show();

                        }

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SignInDialog.this.getDialog().cancel();
            }
        });

        return builder.create();
    }

}
