package com.example.aleksi.harkkat;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aleksi.harkkat.dialogs.SimpleDialog;
import com.example.aleksi.harkkat.fragments.ParticipationFragment;
import com.example.aleksi.harkkat.fragments.ReservationFragment;
import com.example.aleksi.harkkat.fragments.RoomFragment;
import com.example.aleksi.harkkat.user.User;
import com.example.aleksi.harkkat.user.UserController;
import com.example.aleksi.harkkat.fragments.UserFragment;

import org.joda.time.DateTime;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
        /*,SimpleDialog.SimpleDialogListener*/ {
    ParticipationFragment participationFragment;
    private DrawerLayout drawer;
    private User user;
    FragmentManager fragmentManager;
    ReservationFragment reservationFragment;
    UserFragment userFragment;
    RoomFragment roomFragment;
    Toolbar tb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user = UserController.getInstance().getUserByID(getIntent().getExtras().getInt("userID"));


        setupFragments();
        setupToolbar();


        NavigationView nv = findViewById(R.id.nav_view);
        nv.setNavigationItemSelectedListener(this);
        TextView nav_name = (TextView)nv.getHeaderView(0).findViewById(R.id.nav_name);
        nav_name.setText(user.getName());
        if (!user.isSuperUser())
            nv.getMenu().setGroupVisible(R.id.group_1, false);


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    participationFragment).commit();
            Toast.makeText(MainActivity.this, "Welcome, " + user.getName(), Toast.LENGTH_LONG).show();
            nv.setCheckedItem(R.id.nav_participation);
            tb.setTitle(R.string.drawer_menu_participation);
        }
    }

    private void setupToolbar() {
        tb = findViewById(R.id.toolbar);
        setSupportActionBar(tb);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, tb, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    public User getUser() {
        return user;
    }

    private void setupFragments() {
        fragmentManager = getSupportFragmentManager();


        if (participationFragment == null) {
            participationFragment = new ParticipationFragment();
        }

        if (reservationFragment == null) {
            reservationFragment = new ReservationFragment();
        }


        if (userFragment == null) {
            userFragment = new UserFragment();
        }
        if (roomFragment == null) {
            roomFragment = new RoomFragment();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {

            case R.id.nav_participation:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        participationFragment).commit();
                tb.setTitle(R.string.drawer_menu_participation);

                break;


            case R.id.nav_reservation:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        reservationFragment).commit();
                tb.setTitle(R.string.drawer_menu_reservation);
                break;


            case R.id.nav_user:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        userFragment).commit();
                tb.setTitle(R.string.drawer_menu_users);
                break;
            case R.id.nav_room:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        roomFragment).commit();
                tb.setTitle(R.string.drawer_menu_room);
                break;
            case R.id.nav_logout:

                Intent intent = new Intent(MainActivity.this, LoginActivity.class);

                finish();
                startActivity(intent);

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;


    }
/*
    @Override
    public void onDialogPositiveClick(DateTime start, DateTime end, int capacity, String name) {

        reservationFragment.onDialogPositiveClick(start,end,capacity,name);
    }
*/
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);

        } else {
            super.onBackPressed();
        }
    }


}
