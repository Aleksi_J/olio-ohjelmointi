/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko2;

import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 *
 * @author aleksi
 */
public class Dog {
    
    private String name = "";
    private String voice = "";

    Dog(String n) {
        if (n.trim().isEmpty()) {
            name = "Doge";
        } else {
            name = n;
        }
        
    }
    
    public void setName(String n) {
        name = n;
    }
    public String getName() {
        return name;
    }
    public void setVoice(String v) {
        voice = v;
    }
    public void speak(String v) {
        Scanner scan = new Scanner(v);
        String alku = "";
        while (scan.hasNext()) {
            if (scan.hasNextInt()) {
                alku = "Such integer: ";
            } else if (scan.hasNextBoolean()){
                alku = "Such boolean: ";
            } else {
                alku = "";
            }
            System.out.println(alku + scan.next());
            
        }
    }
}
