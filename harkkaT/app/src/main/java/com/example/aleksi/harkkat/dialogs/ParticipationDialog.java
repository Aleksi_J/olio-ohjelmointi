package com.example.aleksi.harkkat.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.aleksi.harkkat.R;
import com.example.aleksi.harkkat.reservation.Reservation;
import com.example.aleksi.harkkat.reservation.ReservationController;
import com.example.aleksi.harkkat.user.UserController;


/*Prints participants to alert dialog.*/
public class ParticipationDialog extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_textview, null);
        TextView txt = (TextView)view.findViewById(R.id.dialog_txt);

        int eventID = getArguments().getInt("eventID");
        int roomID = getArguments().getInt("roomID");
        ReservationController rc = ReservationController.getInstance();
        Reservation reservation = rc.getRoom(roomID).getResByID(eventID);
        txt.setText("");
        UserController uc = UserController.getInstance();
        for (int id : reservation.getParticipants()) {
            txt.append(uc.getUserByID(id).getName() + "\n");
        }





        builder.setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        return builder.create();
    }
}
