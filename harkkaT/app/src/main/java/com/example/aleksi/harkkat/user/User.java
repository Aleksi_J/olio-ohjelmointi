package com.example.aleksi.harkkat.user;


import java.util.ArrayList;

public class User {
    private String name;
    private int userID;
    private boolean frozen;
    private boolean superUser = false;
    private String passwd;
    private static ArrayList<Integer> userIDList = new ArrayList<>();
    private static int idIterator = 1;

    public User(String name, boolean frozen, String passwd) {
        this.name = name;
        this.frozen = frozen;
        this.passwd = passwd;
        this.userID = setID();
    }

    public User(String name, boolean frozen, boolean superUser, String passwd) {
        this.name = name;
        this.frozen = frozen;
        this.superUser = superUser;
        this.passwd = passwd;
        this.userID = setID();
    }

    public User(String name, int userID, boolean frozen, boolean superUser, String passwd) {
        this.name = name;
        this.userID = userID;
        this.frozen = frozen;
        this.superUser = superUser;
        this.passwd = passwd;
        userIDList.add(userID);
    }

    public boolean isSuperUser() {
        return superUser;
    }

    private int setID() {
        while (userIDList.contains(idIterator)) {
            idIterator++;
        }
        userIDList.add(idIterator);
        return idIterator;
    }

    public void checkID() {
        if(!userIDList.contains(this.userID)) {
            userIDList.add(this.userID);
        }
    }

    public String getPasswd() {
        return passwd;
    }

    public int validate(String name, String passwd) {
        if ((this.name.equals(name)) && (this.passwd.equals(passwd))) {
            return userID;
        }
        return 0;
    }

    public int getUserID() {
        return userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSuperUser(boolean superUser) {
        this.superUser = superUser;
    }

    public boolean isFrozen() {
        return frozen;
    }

    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Override
    public String toString() {
        return name;
    }
}
