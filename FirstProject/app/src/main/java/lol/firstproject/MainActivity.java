package lol.firstproject;

import android.app.Activity;
import android.content.Context;
import android.icu.util.Output;
import android.os.Bundle;
import android.service.autofill.TextValueSanitizer;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Arrays;

public class MainActivity extends Activity {
    TextView txt;
    Context context = null;
    EditText etxt;
    EditText tdNimi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txt =  (TextView) findViewById(R.id.textView);
        context = MainActivity.this;
        System.out.println(context.getFilesDir());
        etxt = (EditText) findViewById(R.id.editText5);
        tdNimi = (EditText) findViewById(R.id.editText);

    }

    public void changeText(View v) {
        txt.setText("Hello World!");
        System.out.println("Hello World!");

    }
    public void readFile(View v) {
        try {
            InputStream ins = context.openFileInput(tdNimi.getText().toString()); //TODO tälle arvo!!!
            BufferedReader br = new BufferedReader(new InputStreamReader(ins));
            String s = "";
            String l = "";
            while ((s = br.readLine()) != null) {
                System.out.println(s);
                l += s;
            }
            etxt.setText(l);
            ins.close();

        } catch (IOException e) {
            Log.e("IOException", "Virhe syötteessä");
        } finally {
            System.out.println("Luettu.");
        }

    }
    public void writeFile(View v) {

        try {
            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput(tdNimi.getText().toString(), Context.MODE_PRIVATE));

            String s = etxt.getText().toString();

            ows.write(s);
            ows.close();

        } catch (IOException e) {
            Log.e("IOException", "Virhe syötteessä");
        } finally {
            System.out.println("Kirjoitettu.");
        }
    }
}
