package com.example.aleksi.harkkat.fragments;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aleksi.harkkat.R;
import com.example.aleksi.harkkat.dialogs.ParticipationDialog;
import com.example.aleksi.harkkat.exceptions.DuplicateException;
import com.example.aleksi.harkkat.json.JSONWriterReservation;
import com.example.aleksi.harkkat.reservation.ReservationController;
import com.example.aleksi.harkkat.reservation.RoomReservationController;
import com.example.aleksi.harkkat.user.User;
import com.example.aleksi.harkkat.user.UserController;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;


/*Fragment that ables joining to event. In this fragment, you are also able too see whom had
* participated to your event.*/
public class ParticipationFragment extends Fragment implements RecycleViewAdapt.ItemClickListener {

    private RecyclerView rcv;
    private ReservationController rc;
    private Spinner spinner;
    private RecycleViewAdapt rva;
    private int spinnerPosition;
    private int recyclerPosition;
    private User user;

    private Date pv;
    private Button dateButton;
    private ArrayAdapter<RoomReservationController> adapter;
    private TextView capLoc;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        user = UserController.getInstance().getUserByID(getActivity().getIntent().getExtras().getInt("userID"));
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        dateButton = (Button) view.findViewById(R.id.button2);
        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDateClick();
            }
        });
        capLoc = (TextView)view.findViewById(R.id.par_caploc);
        Button removeButton = (Button) view.findViewById(R.id.buttonRemoveP);

        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeParticipation();
                adapter.notifyDataSetChanged();
            }
        });


        Button addButton = (Button) view.findViewById(R.id.buttonAddP);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addParticipation();
                adapter.notifyDataSetChanged();
            }
        });



        rcv = (RecyclerView) view.findViewById(R.id.hom_rec_view);
        rc = ReservationController.getInstance();
        rcv.setLayoutManager(new LinearLayoutManager(getContext()));
        spinner = (Spinner) view.findViewById(R.id.roomSpinner);
        if(pv==null)
            pv = DateTime.now().toDate();
        this.initResSpinner(spinner);

        final DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM");
        dateButton.setText(fmt.print(new DateTime(pv)));


        return view;
    }

    /* Changes showing reservations by day.*/
    public void onDateClick() {
        Calendar cal = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(year,month,dayOfMonth);
                pv = new DateTime(cal).toDate();

                changeResList();
                final DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM");
                dateButton.setText(fmt.print(new DateTime(pv)));
            }
        },cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();


    }

    /*initialize top spinner to see available rooms.*/
    private void initResSpinner(final Spinner spinner) {
        adapter = new ArrayAdapter<>(getContext().getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item, ReservationController.getRoomList());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.notifyDataSetChanged();
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerPosition = position;
                RoomReservationController item = (RoomReservationController) parent.getSelectedItem();
                changeResList();
                //Changes top-left textview
                capLoc.setText(R.string.Location);
                capLoc.append(" " + item.getLocation()+ "\n");
                capLoc.append(getResources().getString(R.string.capacity));
                capLoc.append(" " + Integer.toString(item.getMaxCapacity()));


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    public void removeParticipation() {
        ReservationController rc = ReservationController.getInstance();
        if(rc.getRoom(adapter.getItem(spinnerPosition).getRoomID())
                .removeParticipation(rva.getItem(recyclerPosition).getEventID(),
                        user.getUserID())) {
            changeResList();
            try {
                JSONWriterReservation.writeReservationStream(rc.getRoomList(), getContext());
            } catch (IOException e) {
                Toast.makeText(getContext(),R.string.write_failed, Toast.LENGTH_SHORT).show();
            }
            Toast.makeText(getContext(), R.string.part_remove_pass, Toast.LENGTH_LONG).show();
        }  else {
            Toast.makeText(getContext(), R.string.remove_failure, Toast.LENGTH_LONG).show();
        }


    }

    public void addParticipation() {
        ReservationController rc = ReservationController.getInstance();
        try {
            rc.getRoom(adapter.getItem(spinnerPosition).getRoomID()).participateToEvent(rva.getItem(recyclerPosition).getEventID(),user.getUserID());
            JSONWriterReservation.writeReservationStream(rc.getRoomList(),getContext());
            changeResList();
        } catch (IOException e) {
            Toast.makeText(getContext(),R.string.write_failed, Toast.LENGTH_SHORT).show();
        } catch (IndexOutOfBoundsException e) {
            Toast.makeText(getContext(), R.string.part_full, Toast.LENGTH_LONG).show();
        } catch (DuplicateException e) {
            Toast.makeText(getContext(), R.string.part_already, Toast.LENGTH_LONG).show();
        }
    }

    /*Updates reservationlist.*/
    public void changeResList() {
        rva = new RecycleViewAdapt(rc.getRoom(adapter.getItem(spinnerPosition).getRoomID()).getReservationsByTime(new DateTime(pv)), user.getUserID());
        rcv.setAdapter(rva);
        rcv.setItemAnimator(new DefaultItemAnimator());
        rva.setClickListener(this);
    }

    /*If user is superuser or reserver of the event, by clicking the event shows its participants.*/
    public void onItemClick(View v, int position) {
        recyclerPosition = position;
        if (user.getUserID() == rva.getItem(recyclerPosition).getReserverID() || user.isSuperUser()) {
            ParticipationDialog dialog = new ParticipationDialog();
            Bundle args = new Bundle();
            args.putInt("eventID", rva.getItem(recyclerPosition).getEventID());
            args.putInt("roomID", adapter.getItem(spinnerPosition).getRoomID());
            dialog.setArguments(args);
            dialog.show(getActivity().getSupportFragmentManager(),"lol");
        }

    }
}