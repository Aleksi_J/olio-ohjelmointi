package com.example.aleksi.viikko11;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
SettingsFragment.OnSettingsFragmentListener{
    private DrawerLayout drawer;
    HomeFragment mHomeFragment;
    SettingsFragment mSettingsFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fragmentManager = getSupportFragmentManager();
        mSettingsFragment = (SettingsFragment) fragmentManager.findFragmentByTag("settings");
        if (mSettingsFragment == null) {
            mSettingsFragment = new SettingsFragment();
        }
        mHomeFragment = (HomeFragment) fragmentManager.findFragmentByTag("home");
        if (mHomeFragment == null) {
            mHomeFragment = new HomeFragment();
        }
        setContentView(R.layout.activity_main);
        Toolbar tb = findViewById(R.id.toolbar);
        setSupportActionBar(tb);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, tb, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        NavigationView nv = findViewById(R.id.nav_view);
        nv.setNavigationItemSelectedListener(this);
        toggle.syncState();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,
                    mHomeFragment).commit();
            System.out.println("tsekkaa tää");
            nv.setCheckedItem(R.id.nav_home);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        mSettingsFragment).commit();
                break;
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        mHomeFragment).commit();
                break;

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;


    }

    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);

        } else {
            super.onBackPressed();
        }
    }
    public void changeFont(int i) {
        mHomeFragment.setFontSize(i);

    }

    public void maxLines(int i) {
        mHomeFragment.setMaxLines(i);

    }
    public void capitalize(boolean isChecked) {
        mHomeFragment.capitalize(isChecked);
    }

    public void setItalic(boolean isChecked) {
        mHomeFragment.setItalic(isChecked);
    }
    public void setEditable(boolean isChecked) {
        mHomeFragment.setEditable(isChecked);
    }

    public void setDisplayText(String s) {
        mHomeFragment.setDText(s);
    }
    public void changeLang(String s) {
        setLocale(s);
    }
    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, MainActivity.class);
        finish();
        startActivity(refresh);

    }


}
